var reporter = require('cucumber-html-reporter')
const dotenv = require('dotenv')
dotenv.config()

var options = {
  theme: 'bootstrap',
  jsonFile: 'output/output.json',
  output: 'output/report.html',
  reportSuiteAsScenarios: true,
  scenarioTimestamp: true,
  launchReport: false,
  brandTitle: 'Cucumber automatic tests',
  metadata: {
    "GitLab job number": `<a href="${process.env.CI_JOB_URL}">${process.env.CI_JOB_ID}</a>`,
    "GitLab pipeline": `<a href="${process.env.CI_PIPELINE_URL}">${process.env.CI_PIPELINE_ID}</a>`
  },
}

reporter.generate(options)