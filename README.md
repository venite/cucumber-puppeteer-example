# cucumber-puppeteer-examples
These are a few examples of how to use Cucumber and combine it with Puppeteer to run automated tests in Chrome. It uses [Cucumber HTML Reporter](https://www.npmjs.com/package/cucumber-html-reporter) to generate an HTML report of the test results. The result of the latest run on GitLab can be viewed on [GitLab Pages](https://venite.gitlab.io/cucumber-puppeteer-example/).

The suite currently tests the name of the google.com website and the functionality of [Quotes to Scrape](http://quotes.toscrape.com/), two sites who don't mind you using automated tests against them.


## How to run
1. Install dependencies with `npm ci`.
2. Run the testsuite with `npm test`.
3. Generate the HTML report with `node generate-report.js`.
4. Post to a slack channel with `node post-to-slack.js`. (Make sure to get a webhook for your channel and set the `SLACK_WEBHOOK_URL` environment variable in your .env file.)

## How it works

### How tests are executed
Cucumber will read the `.feature` files from the `features` directory. It will then match these to the functions in the `step_definitions` directory. The name of the step definition file does not have to match the feature file. Cucumber will find it if the first string of the method matches the step in the feature.

### Hooks
If you find yourself repeating code before and after every test, you can add it to the `hooks.js` file. As an example, we use this to take a screenshot when a test fails in the browser. Note that `BeforeAll` and `AfterAll` do _not_ have access to `this`.

### How to add tests
Add a new `.feature` file with a nice, descriptive name. In it, declare the `Feature:` and at least one `Scenario:`. Start with a `Given` or `When`. Save the file and run `npm test`. Cucumber will now complain that you haven't implemented the step yet, and even provide some nice boilerplate code for it. Copy this to its own `.js` file in the `step_definitions` directory and implement it. Repeat this with more `Given`, `When` or `Then` steps for every step of your scenario.

Every step should have a unique name, and `Given` and `When` can be used interchangeably. If two steps in different scenarios share a name, Cucumber will use the same implementation.


### What to do if the tests fail, if they used to pass
1. Run them again. End-to-end tests are brittle.
2. Find the step definition of the culprit. Does it contain a CSS selector? Has the name or visibility of the object it refers to changed?
3. If there are no obvious changes or mistakes, add 
    ```
    await this.page.screenshot({path: 'descriptiveName.png'})
    ```
    to take a screenshot.
4. If you want to see the entire scenario, launch the browser with head. This will make the tests run too quickly to see anything, so add the slowMo-option: 
    ```
    const browser = await puppeteer.launch({
        headless: false,
        slowMo: 250
    });
    ```
    If your tests now magically pass, the slowMo has probably prevented a race condition. 

## Thanks
to my employer, [Infi](https://www.infi.nl), for giving me the time to work on this and permission to open-source it.