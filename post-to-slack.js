const dotenv = require('dotenv')
dotenv.config()

const https = require('https')
const cucumberReport = require('./output/output.json')

const webHookURL = process.env.SLACK_WEBHOOK_URL

/**
 * Extract the relevant data from the Cucumber report.
 * @param report
 */
function analyzeReport(report) {
  let slackBlocks = [
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: `Cucumber test summary for pipeline <${process.env.CI_PIPELINE_URL}|${process.env.CI_PIPELINE_IID}>. <https://venite.gitlab.io/cucumber-puppeteer-example/|view it in the browser>`,
      },
    },
  ]

  // Loop over the report and count the failed, skipped and passed tests
  for (let feature in report) {
    let passedSteps = 0
    let failedSteps = 0
    let skippedSteps = 0
    let failedStepName = null
    let featureName = report[feature].name
    for (let scenario in report[feature].elements) {
      let thisScenario = report[feature].elements[scenario]
      for (let step in thisScenario.steps) {
        thisStep = thisScenario.steps[step]
        if (thisStep.keyword === 'Before' || thisStep.keyword === 'After') {
          // Don't analyze the setup steps
          continue
        }
        if (thisStep.result.status === 'passed') {
          passedSteps++
        } else if (thisStep.result.status === 'skipped') {
          skippedSteps++
        } else {
          failedStepName = thisStep.keyword + thisStep.name
          failedSteps++
        }
      }
    }

    const failedText =
      failedSteps > 0
        ? `:x: ${failedSteps}, :grey_question: ${skippedSteps}. Example failed step: _${failedStepName}_`
        : ''

    const passedText =
      passedSteps > 0 ? `:heavy_check_mark: ${passedSteps}` : ''

    slackBlocks.push({
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: `${featureName}: ${passedText} ${failedText}`,
      },
    })
  }

  return slackBlocks
}

/**
 * Prepare the message to send to Slack
 * @param {JSON} messageBlocks
 * @returns {JSON}
 */
function prepareSlackMessage(messageBlocks) {
  const message = {
    username: 'Cucumber/Puppeteer NotifyBot',
    icon_emoji: ':cucumber:',
    blocks: messageBlocks,
  }

  return message
}

/**
 * Handles the actual sending request.
 * We're turning the https.request into a promise here for convenience
 * @param webhookURL
 * @param messageBody
 * @return {Promise}
 */
function sendSlackMessage(webhookURL, messageBody) {
  // make sure the incoming message body can be parsed into valid JSON
  try {
    messageBody = JSON.stringify(messageBody)
  } catch (e) {
    throw new Error('Failed to stringify messageBody', e)
  }

  return new Promise((resolve, reject) => {
    const requestOptions = {
      method: 'POST',
      header: {
        'Content-Type': 'application/json',
      },
    }

    const req = https.request(webhookURL, requestOptions, res => {
      let response = ''

      res.on('data', d => {
        response += d
      })

      res.on('end', () => {
        resolve(response)
      })
    })

    // there was an error, reject the promise
    req.on('error', e => {
      reject(e)
    })

    req.write(messageBody)
    req.end()
  })
}

// main
(async function() {
  if (!webHookURL) {
    console.error('Is the SLACK_WEBHOOK_URL environment variable set?')
  }
  const reportData = analyzeReport(cucumberReport)
  const message = prepareSlackMessage(reportData)
  try {
    await sendSlackMessage(webHookURL, message)
  } catch (e) {
    console.error('There was an error with the request', e)
  }
})()
