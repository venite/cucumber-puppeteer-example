import { setWorldConstructor, setDefaultTimeout } from 'cucumber'
import dotenv from 'dotenv';

dotenv.config();

setDefaultTimeout(30 * 1000) // wait max 30 seconds

// Use world to set shared state for tests such as base url.
class World {
  constructor({attach}) {
    this.attach = attach
    this.quotesUrl = 'http://quotes.toscrape.com'
  }
}

setWorldConstructor(World)
