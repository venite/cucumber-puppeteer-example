import { assert } from "chai";
import { When, Then } from 'cucumber'

When('I visit {string}', async function(url) {
  await this.page.goto(url)
})

Then('the page title should be {string}', async function(expectedTitle) {
  const title = await this.page.title()
  assert.equal(title, expectedTitle, `The title was expected to be ${expectedTitle} but was ${title}.`)
})
