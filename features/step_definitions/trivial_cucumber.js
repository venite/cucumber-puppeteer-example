import { Given, When, Then } from "cucumber";
import { assert } from "chai";

Given("something that doesn't matter", function() {
  // Here you would do setup for the next steps. If it mattered.
});

Then("the string {string} equals the different string {string}", function(
  string,
  string2
) {
    // this test will pass or fail depending on the result of the assert.
    assert.equal(string, string2);
});

Then('this condition will not be reached', function () {
    // Even though this returns false, it will not be reported as such, because the 
    // method is not executed.
    return false;
  });

Then("this method is pending", function() {
  // returning "pending" signals to cucumber that this method hasn't been implemented yet.
  return "pending";
});
