import { Given, When, Then } from "cucumber";
import { assert } from "chai";

Given("I visit the Quotes To Scrape homepage", async function() {
  await this.page.goto(this.quotesUrl);
});

When("I click the {string} tag", async function(tagName) {
  // Declare the navigation promise. We do this BEFORE navigating.
  const navigationPromise = this.page.waitForNavigation();
  await this.page.evaluate(tagName => {
    document.querySelectorAll(".tag-item > .tag").forEach(a => {
      if (a.textContent === tagName) {
        a.click();
        return;
      }
    });
  }, tagName);
  // Now let the navigation promise resolve. This ensures the page has 
  // navigated at the end of this step.
  await navigationPromise;
});

Then("I see the text Viewing tag: {string}", async function(expectedName) {
  const tagName = await this.page.evaluate(() => {
    // This is one way to select a single element.
    return document.querySelector("body > div > h3 > a").text;
  });
  assert.equal(tagName, expectedName);
});

Then("the visible quotes have tag {string}.", async function(tagName) {
  // count the number of visible quotes
  const nrOfQuotes = (await this.page.$$(".quote")).length;

  // We're actually not checking whether every quote has the right tag
  // once, but only if there are as many tags as quotes. Don't tell anyone.
  var nrOfTags = await this.page.evaluate(tagName => {
    var nrOfTags = 0;
    document.querySelectorAll(".quote .tags .tag").forEach(a => {
      if (a.textContent === tagName) {
        nrOfTags++;
      }
    });
    return nrOfTags;
  }, tagName);

  assert.equal(nrOfTags, nrOfQuotes);
});
