Feature: Trivial Puppeteer examples
   These either pass, fail or are undefined.

   Scenario: Visit a web page
      Given I visit 'https://www.google.com'
      Then the page title should be 'Google'

   Scenario: Failing scenario - check out the screenshot!
      Given I visit 'https://www.google.com'
      Then the page title should be 'Definitely Not This'