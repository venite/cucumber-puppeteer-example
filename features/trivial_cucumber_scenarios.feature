Feature: Trivial Cucumber examples
   These either pass, fail or are undefined.

   Scenario: Text equals text
      Given something that doesn't matter
      Then the string 'text' equals the different string 'text'

   Scenario: Text does not equal text (this will fail)
      Given something that doesn't matter
      # This will use the same implementation as above, because the quoted texts are read as variables.
      Then the string 'text' equals the different string 'text2'
      And this condition will not be reached

   Scenario: Pending
      Given something that doesn't matter
      Then this method is pending