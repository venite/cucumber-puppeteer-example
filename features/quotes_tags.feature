Feature: Quotes can be filtered by tag
   I can filter quotes by tag.

   Scenario: Filter quotes by tag on the homepage
      Given I visit the Quotes To Scrape homepage
      When I click the 'love' tag
      Then I see the text Viewing tag: 'love'
      And the visible quotes have tag 'love'.

   Scenario: Filter quotes by tag after they've been filtered already
      Given I visit the Quotes To Scrape homepage
      And I click the 'love' tag
      # This will use the same implementation as above, because the quoted texts are read as variables.
      When I click the 'inspirational' tag
      Then I see the text Viewing tag: 'inspirational'
      And the visible quotes have tag 'inspirational'.